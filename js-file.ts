var level:number = 0;

function start(){
	level=0;
	document.getElementById("game").style.display="";
    document.getElementById("win").style.display="none";
	document.getElementById("lost").style.display="none";
	var left:HTMLElement = document.getElementById("left");
	var right:HTMLElement = document.getElementById("right");
	document.getElementById("level").innerHTML="Level 0";
	left.innerHTML="";
	right.innerHTML="";
	var img0:HTMLImageElement = document.createElement("img");
	var img1:HTMLImageElement = document.createElement("img");
	var x:number=Math.random()*550;
    x=Math.round(x);
    var y:number=Math.random()*540;
	y=Math.round(y);
		
    img0.style.left=x+'px';
	img0.style.top=y+'px';
    img0.src="./tiger.png";
    img0.alt="my Image";
    img0.style.width="100px";
	img0.style.height="100px";
	img0.id="img0";
	img0.style.position="absolute";
	img0.addEventListener("click",lost);
	left.append(img0);

	img1.style.left=x+766+'px';
	img1.style.top=y+'px';
    img1.src="./tiger.png";
    img1.alt="my Image";
    img1.style.width="100px";
	img1.style.height="100px";
	img1.id="img1";
	img1.style.position="absolute";
	img1.addEventListener("click",lost);
	right.append(img1);
	
	var x:number=Math.random()*550;
	x=Math.round(x);
	var y:number=Math.random()*540;
	y=Math.round(y);

	var img2:HTMLImageElement = document.createElement("img");
	img2.style.left=x+'px';
	img2.style.top=y+'px';
	img2.src="./tiger2.png";
    img2.alt="Different Image";
    img2.style.width="100px";
	img2.style.height="100px";
	img2.id="img2";
	img2.style.position="absolute";
	img2.addEventListener("click",myFunction);
	left.append(img2);
}

function myFunction() {
	level++;
	if(level===5){
		document.getElementById("win").style.display="";
		document.getElementById("game").style.display="none";
	}
	document.getElementById("level").innerHTML="Level "+ level;
	var left:HTMLElement = document.getElementById("left");
	var right:HTMLElement = document.getElementById("right");
	left.innerHTML="";
	right.innerHTML="";
	for (var i = 0; i <= level; i++) {
		var img0:HTMLImageElement = document.createElement("img");
		var img1:HTMLImageElement = document.createElement("img");
		var x:number=Math.random()*550;
    	x=Math.round(x);
    	var y:number=Math.random()*540;
    	y=Math.round(y);
		
    	img0.style.left=x+'px';
		img0.style.top=y+'px';
    	img0.src="./tiger.png";
    	img0.alt="my Image";
    	img0.style.width="100px";
		img0.style.height="100px";
		img0.id="img0"+i;
		img0.style.position="absolute";
		img0.addEventListener("click",lost);
		left.append(img0);

		img1.style.left=x+766+'px';
		img1.style.top=y+'px';
    	img1.src="./tiger.png";
    	img1.alt="my Image";
    	img1.style.width="100px";
		img1.style.height="100px";
		img1.id="img1"+i;
		img1.style.position="absolute";
		img1.addEventListener("click",lost);
		right.append(img1);
	};
	var x:number=Math.random()*550;
	x=Math.round(x);
	var y:number=Math.random()*540;
	y=Math.round(y);

	var img2:HTMLImageElement = document.createElement("img");
	img2.style.left=x+'px';
	img2.style.top=y+'px';
	img2.src="./tiger2.png";
    img2.alt="Different Image";
    img2.style.width="100px";
	img2.style.height="100px";
	img2.id="img2";
	img2.style.position="absolute";
	img2.addEventListener("click",myFunction);
	left.append(img2);	
}

function lost(){
	level=0;
	document.getElementById("lost").style.display="";
    document.getElementById("game").style.display="none";
}